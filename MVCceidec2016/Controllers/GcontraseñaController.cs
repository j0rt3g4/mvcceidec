﻿using System.Web.Mvc;
using MVCceidec2016.Models;

namespace MVCceidec2016.Controllers
{
    public class GcontraseñaController : Controller
    {
        // GET: Gcontraseña
        public ActionResult Index()
        {
            Gcontraseña gc = new Gcontraseña();
            return View(gc);
        }

        [HttpPost]
        public ActionResult Index(Gcontraseña gc)
        {
            if (! (gc.cnt > gc.longitud) ) //cnt no puede ser más grande que el tamaño de la contraseña
            {
                gc.pwd = Gcontraseña.GenerarContraseña(gc.longitud, gc.cnt);
                return View(gc);
            }
            //Si llegamos este lejos es porque cnt es más grande que el tamaño de la contraseña
            //creo el mensaje de error
            ViewBag.DetalleDelError = "La longitud de los caracteres especiales no puede ser mayor al numero de digitos de la contraseña";
            return View("Error");
        }

        public ActionResult ActualizarC(Gcontraseña gc)
        {
            if (!(gc.cnt > gc.longitud)) //cnt no puede ser más grande que el tamaño de la contraseña
            {
                gc.pwd = Gcontraseña.GenerarContraseña(gc.longitud, gc.cnt);
                return PartialView("_ActualizarC", gc);
            }
            ViewBag.DetalleDelError = "No se puede actualizar la contraseña, Function:Actualizar, Action: ActualizarC. <br /> La longitud de los caracteres especiales no puede ser mayor al numero de digitos de la contraseña";
            return View("Error");
        }
        
    }
}