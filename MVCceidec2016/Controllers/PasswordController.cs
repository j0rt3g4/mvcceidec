﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCceidec2016.Models;
using Newtonsoft.Json;

namespace MVCceidec2016.Controllers
{
    public class PasswordController : Controller
    {
        // GET: Password
        public ActionResult Index()
        {
            Password pass = new Password();
            Password pass2 = new Password(20);

            ViewBag.Titulo = "Soy el titulo";
            ViewBag.SubTitulo = "Soy el subtitulo";

            return View(pass2);
        }

        public ActionResult Error()
        {
            return View("Error");
        }

        public ActionResult Json()
        {
            Password pwd2 = new Password(25);
            string serializado = JsonConvert.SerializeObject(pwd2);
            return Json(serializado, JsonRequestBehavior.AllowGet);
        }
    }
}