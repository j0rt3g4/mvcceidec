﻿using System;
using System.Collections.Generic;
using System.ComponentModel; //displayname
using System.ComponentModel.DataAnnotations; //display
using System.Linq;
using System.Web;
using System.Web.Razor.Generator;
using System.Web.Security;

namespace MVCceidec2016.Models
{
    public class Password
    {
        //propiedad
        [DisplayName("Contraseña")]
        [Required(ErrorMessage = "Mensaje personalizado de error")]
        public string pwd { get; set; }

        //metodo
        public string GeneraContraseña()
        {
            return Membership.GeneratePassword(15, 3);
        }
        //metodo sobrecargado
        public string GeneraContraseña(int digitos)
        {
            return Membership.GeneratePassword(digitos, 3);
        }


        /*Constructores*/
        //Por defecto
        public Password()
        {
            pwd = GeneraContraseña();
        }

        //Recibiendo un entero
        public Password(int ab)
        {
            pwd = GeneraContraseña(ab);
        }
    }
}