﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCceidec2016.Models
{
    public class Gcontraseña : Password //hereda de password
    {
        //agrega la propiedad longitud
        [Required(ErrorMessage = "Se requiere que se de un número para los dígitos de la contraseña")]
        [DisplayName("Digitos:")]
        [Range(1, 128)]
        public int longitud { get; set; }

        //agrega la propiedad cnt
        [Required(ErrorMessage = "Se requiere que se de un número para la cantidad de caracteres no triviales")]
        [Range(0,100)]
        [DisplayName("Digitos Esp:")]
        public int cnt { get; set; }


        //agrega sobrecarga al método GenerarContraseña (recibe 2 enteros), no esta en la clase base (Password) sólo en esta.
        public static string GenerarContraseña(int l, int c)
        {
            return System.Web.Security.Membership.GeneratePassword(l, c);
        }
        public Gcontraseña()
        {
            //Se inicializan a 1 ya que para generar contraseña se requiere que la longitud y el cnt sea de "al menos" 1.
            longitud = 25;
           // cnt = 1;
            pwd = GenerarContraseña(longitud, cnt);
        }
    }
}